import React from 'react';
import { Text, View, ScrollView, Alert, Button, FlatList, SafeAreaView, TouchableOpacity, Image } from 'react-native'
import { Collapse, CollapseHeader, CollapseBody } from 'accordion-collapse-react-native'
//import { SQLite } from 'expo';
import axios from 'axios';
import i18n from 'i18n-js';
import Icon from 'react-native-vector-icons/FontAwesome5'



export default class MarkerList extends React.Component {
    constructor(props) {
        super(props);

        this.fetchData = this.fetchData.bind(this);

        //console.log(props);
        this.state = {
            dataList: [],
            refreshing: false,
            idUser: props.route.params.idUser
        }

    }

    async componentDidMount() {
        console.log(this.state.idUser);
        await this.fetchData();
    }

    fetchData() {
        axios({
            method: 'POST',
            url: 'http://192.168.1.5:3000/getmarkersbyuser',
            data:{
                idUser: this.state.idUser
              }
          })
          .then((res)=>{
            this.setState({dataList: res.data}), () => console.log("AQUI" + this.state.dataList.nameUser + "FINITO");
            console.log("hey there" + this.state.dataList.nameUser);
          }, (error)=>{
            console.log('ups' + error);
          })
    }

    handleRefresh = () => {
        this.setState({ dataList: [], refreshing: true }, () => {
            axios({
                method: 'POST',
                url: 'http://192.168.1.5:3000/getmarkersbyuser',
                data:{
                    idUser: this.state.idUser
                  }
              })
              .then((res)=>{
                //this.setState({points: res.data}), () => console.log("AQUI" + this.state.dataList + "FINITO");
                console.log("hey there");
              }, (error)=>{
                console.log('ups' + error);
              })
        })
    }

    async handleDelete(id) {
        await this.deleteData(id);
        this.fetchData();
    }

    deleteData(id) {
        axios({
            method: 'POST',
            url: 'http://192.168.1.5:3000/deletemarker',
            data:{
                id: id
              }
          })
          .then((res)=>{
            alert("Ponto apagado");
          }, (error)=>{
            console.log('ups' + error);
          })
    }

    render() {

        // if (this.state.loading) {
        //     return (
        //         <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
        //             <Spinner color="orange"></Spinner>
        //         </View>
        //     );
        // }

        return (
            <SafeAreaView>
                <ScrollView>
                    <View>
                        <FlatList
                            refreshing={this.state.refreshing}
                            onRefresh={this.handleRefresh}
                            data={this.state.dataList}
                            renderItem={(item) => {
                                return (
                                    <View>
                                        <View style={{ flexDirection: 'row' }}>
                                        <Text style={{marginBottom: 10, height: 120, marginTop: -40}}>
                                      <Image
                                            style={{ width: 110, height: 100, alignSelf: "center"}}
                                            source={{uri: `${item.item.img}`}}
                                        />
                                        </Text> 
                                            <Text style={{ fontSize: 20, marginTop: 10, marginBottom: 5, marginLeft: 24, flex: 8 }}>{item.item.desc}</Text>
                                            <TouchableOpacity style={{ flex: 1 }} title="Update" onPress={() => {
                                                this.props.navigation.navigate('Editar Ponto', {id: item.item._id, desc: item.item.desc, img: item.item.img});
                                            }}>
                                                <Icon style={{ marginTop: 7 }} name={'pencil-alt'} size={20} color={'rgba(100,100,100, 0.7)'} />
                                            </TouchableOpacity>
                                            <TouchableOpacity style={{ flex: 1 }} title="Delete" onPress={() => {
                                                this.handleDelete(item.item._id);
                                            }}>
                                                <Icon style={{ marginTop: 7 }} name={'times'} size={20} color={'rgba(100,100,100, 0.7)'} />
                                            </TouchableOpacity>
                                        </View>
                                        <View style={{ borderBottomColor: 'grey', borderBottomWidth: 1 }}></View>

                                    </View>
                                );
                            }}
                            //keyExtractor={(item) => `${item.id}`}
                        />
                    </View>
                </ScrollView>

            </SafeAreaView >
        )
    }

}