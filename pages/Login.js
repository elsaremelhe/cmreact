import React from 'react';
import { StyleSheet, Text, View, Image, TextInput, Dimensions, TouchableOpacity } from 'react-native';
import i18n from 'i18n-js';
import Trad from './Localization';
import axios from 'axios';
import Icon from 'react-native-vector-icons/Ionicons'
import logoimg from '../images/loginlogo.jpg'
import logoimgLand from '../images/left.jpg'
import * as ScreenOrientation from 'expo-screen-orientation';
import { DeviceMotion } from 'expo-sensors';

const { width: WIDTH } = Dimensions.get('window');

export default class Login extends React.Component {
  constructor(props) {
    super(props)

    this.showPass = this.showPass.bind(this);

    this.state = {
      showPass: true,
      press: false,
      pswd: '',
      email: '',
      currentOrientation: 0
    }

  }

  componentDidMount() {
    ScreenOrientation.unlockAsync();


    DeviceMotion.addListener(({ rotation }) => {
      ScreenOrientation.getOrientationAsync().then(data => this.setState({ currentOrientation: data }));
    })
  }

  showPass = () => {
    if (!this.state.press) {
      this.setState({ showPass: false, press: true })
    } else {
      this.setState({ showPass: true, press: false })
    }
  }


  render() {
    return (

      <View style={styles.background}>
        {this.state.currentOrientation == 1 &&
        <Image source={logoimg} style={styles.logo}></Image>}

        {/* {this.state.currentOrientation != 1 &&
        <Image source={logoimgLand} style={{marginLeft: -450, marginTop: -50, width: 400, height: 450}}></Image>} */}

        {this.state.currentOrientation == 1 &&
        <View>
          <View style={styles.inputContainer}>
            <Icon name={'ios-mail'} size={28} color={'rgba(100,100,100, 0.7)'} style={styles.icons} />
            <TextInput
              style={styles.input}
              placeholder={'Email'}
              placeholderTextColor={'rgba(100,100,100, 0.7)'}
              underlineColorAndroid='transparent'
              onChangeText={(text) => this.setState({ email: text })}
              value={this.state.email} />
          </View>

        <View style={styles.inputContainer}>
          <Icon name={'md-lock'} size={28} color={'rgba(100,100,100, 0.7)'} style={styles.icons} />
          <TextInput
            style={styles.input}
            placeholder={'Password'}
            secureTextEntry={this.state.showPass}
            placeholderTextColor={'rgba(100,100,100, 0.7)'}
            underlineColorAndroid='transparent'
            onChangeText={(text) => this.setState({ pswd: text })}
            value={this.state.pswd} />

          <TouchableOpacity style={styles.inputEye} onPress={this.showPass.bind(this)}>
            <Icon name={this.state.press ? 'md-eye-off' : 'md-eye'} size={26} color={'rgba(100,100,100, 0.7)'} />
          </TouchableOpacity>
        </View>

        <TouchableOpacity style={styles.btnLogin} onPress={() => {
          axios({
            method: 'POST',
            url: 'http://192.168.1.5:3000/login',
            data: {
              email: this.state.email,
              pswd: this.state.pswd
            }
          })
            .then((res) => {
              alert("Login com sucesso");
              this.props.enviarName(res.data.message.name);
              this.props.enviarID(res.data.message.idUser);
              this.props.navigation.replace('Mapa');
              console.log(res.data.message);
              console.log(res.data.message.name);
            }, (error) => {
              console.log('ups' + error);
            })
        }}>
          <Text style={styles.btnText}>Login</Text>
        </TouchableOpacity>

        <View style={{ alignItems: 'center' }}>
          <TouchableOpacity style={{ alignItems: 'center', marginTop: 175, marginBottom: 10 }} onPress={() => {
            this.props.navigation.navigate('Register');
          }}><Text style={{ color: 'rgba(100,100,100, 1)' }}>{Trad.t('newAccount')}</Text></TouchableOpacity>
          <View style={{ flexDirection: 'row', marginBottom: 10 }}>
            <View style={{ borderBottomColor: '#FF7E00', borderBottomWidth: 1, flex: 2, marginBottom: 10, marginLeft: 50 }}></View>
            <Text style={{ color: '#FF7E00', fontSize: 17, flex: 1, textAlign: 'center' }}>{i18n.t('or')}</Text>
            <View style={{ borderBottomColor: '#FF7E00', borderBottomWidth: 1, flex: 2, marginBottom: 10, marginRight: 50 }}></View>
          </View>
          <TouchableOpacity style={{ alignItems: 'center' }} onPress={() => {
            this.props.navigation.navigate('Notas');
          }}><Text style={{ color: 'rgba(100,100,100, 1)' }}>{i18n.t('enterGuest')}</Text></TouchableOpacity>
        </View>
        </View>}

        {this.state.currentOrientation != 1 &&
        <View style={{flexDirection: 'row'}}>
          <Image source={logoimgLand} style={{marginLeft: -100, marginTop: -20}}></Image>
        <View>
          <View style={styles.inputContainer}>
            <Icon name={'ios-mail'} size={28} color={'rgba(100,100,100, 0.7)'} style={styles.icons} />
            <TextInput
              style={styles.input}
              placeholder={'Email'}
              placeholderTextColor={'rgba(100,100,100, 0.7)'}
              underlineColorAndroid='transparent'
              onChangeText={(text) => this.setState({ email: text })}
              value={this.state.email} />
          </View>

        <View style={styles.inputContainer}>
          <Icon name={'md-lock'} size={28} color={'rgba(100,100,100, 0.7)'} style={styles.icons} />
          <TextInput
            style={styles.input}
            placeholder={'Password'}
            secureTextEntry={this.state.showPass}
            placeholderTextColor={'rgba(100,100,100, 0.7)'}
            underlineColorAndroid='transparent'
            onChangeText={(text) => this.setState({ pswd: text })}
            value={this.state.pswd} />

          <TouchableOpacity style={styles.inputEye} onPress={this.showPass.bind(this)}>
            <Icon name={this.state.press ? 'md-eye-off' : 'md-eye'} size={26} color={'rgba(100,100,100, 0.7)'} />
          </TouchableOpacity>
        </View>

        <TouchableOpacity style={styles.btnLoginLan} onPress={() => {
          axios({
            method: 'POST',
            url: 'http://192.168.1.5:3000/login',
            data: {
              email: this.state.email,
              pswd: this.state.pswd
            }
          })
            .then((res) => {
              alert("Login com sucesso");
              this.props.enviarName(res.data.message.name);
              this.props.enviarID(res.data.message.idUser);
              this.props.navigation.replace('Mapa');
              console.log(res.data.message);
              console.log(res.data.message.name);
            }, (error) => {
              console.log('ups' + error);
            })
        }}>
          <Text style={styles.btnText}>Login</Text>
        </TouchableOpacity>

        <View style={{ alignItems: 'center' }}>
          <TouchableOpacity style={{ alignItems: 'center', marginTop: 55, marginBottom: 10 }} onPress={() => {
            this.props.navigation.navigate('Register');
          }}><Text style={{ color: 'rgba(100,100,100, 1)' }}>{Trad.t('newAccount')}</Text></TouchableOpacity>
          <View style={{ flexDirection: 'row', marginBottom: 10 }}>
            <View style={{ borderBottomColor: '#FF7E00', borderBottomWidth: 1, flex: 2, marginBottom: 10, marginLeft: 50 }}></View>
            <Text style={{ color: '#FF7E00', fontSize: 17, flex: 1, textAlign: 'center' }}>{i18n.t('or')}</Text>
            <View style={{ borderBottomColor: '#FF7E00', borderBottomWidth: 1, flex: 2, marginBottom: 10, marginRight: 50 }}></View>
          </View>
          <TouchableOpacity style={{ alignItems: 'center' }} onPress={() => {
            this.props.navigation.navigate('Notas');
          }}><Text style={{ color: 'rgba(100,100,100, 1)' }}>{i18n.t('enterGuest')}</Text></TouchableOpacity>
        </View>
        </View>
        </View>}
      </View>

    );
  }
}


const styles = StyleSheet.create({
  background: {
    flex: 1,
    width: null,
    height: null,
    alignItems: 'center',
    backgroundColor: 'white'
  },
  logo: {
    width: WIDTH,
    marginTop: 0,
    marginBottom: 0,
    height: 300,
    alignItems: 'flex-start'
  },
  inputContainer: {
    marginTop: 15
  },
  input: {
    width: WIDTH - 55,
    height: 45,
    borderRadius: 25,
    fontSize: 16,
    paddingLeft: 45,
    backgroundColor: 'rgba(255,255,255,0.35)',
    color: 'rgba(0,0,0,0.7)',
    marginHorizontal: 25
  },
  icons: {
    position: 'absolute',
    top: 8,
    left: 37
  },
  inputEye: {
    position: 'absolute',
    top: 8,
    right: 37
  },
  btnLogin: {
    width: WIDTH - 205,
    height: 45,
    borderRadius: 25,
    backgroundColor: '#FF7E00',
    justifyContent: 'center',
    marginTop: 30,
    marginLeft: 101
  },
  btnText: {
    color: 'white',
    fontSize: 16,
    textAlign: 'center'
  },
  inputContainerLand: {
    marginTop: 15,
    marginLeft: 100,
    
  },
  btnLoginLan: {
    width: WIDTH - 405,
    height: 45,
    borderRadius: 25,
    backgroundColor: '#FF7E00',
    justifyContent: 'center',
    marginTop: 30,
    marginLeft: 101,
    marginRight: 101
  }
});
