import React from 'react';
import { Text, View, ScrollView, Alert, Button, FlatList, SafeAreaView, TouchableOpacity, InteractionManager } from 'react-native'
import { Collapse, CollapseHeader, CollapseBody } from 'accordion-collapse-react-native'
//import { SQLite } from 'expo';
import i18n from 'i18n-js';
import * as SQLite from 'expo-sqlite';
import Icon from 'react-native-vector-icons/FontAwesome5'
import { useFocusEffect } from '@react-navigation/native'

const db = SQLite.openDatabase('CMNotes');


export default class NewNoteSQLite extends React.Component {
    constructor(props) {
        super(props);

        this.fetchData = this.fetchData.bind(this);

        this.state = {
            dataList: [],
            refreshing: false
        }

        this.props.navigation.setOptions({
            headerRight: () => (<TouchableOpacity style={{ marginRight: 24 }} title="New" onPress={() => {
                this.props.navigation.navigate('Nova Nota');
            }}>
                <Icon name={'plus'} size={20} color={'white'} />
            </TouchableOpacity>)
        })

    }


    async componentDidMount() {
        db.transaction(tx => {
            tx.executeSql(
                'Create table if not exists Notes (id integer primary key not null, title text, description text)'
            );
        }, function (tx, error) {
            Alert.alert("Erro a criar");
        });
        await this.fetchData();

    }

    fetchData() {
        var query = "SELECT * from Notes";
        var params = [];

        db.transaction(tx => {
            tx.executeSql(query, params, (tx, results) => {
                console.log(results);
                this.setState({
                    dataList: results.rows._array
                })
            }, function (tx, error) {
                Alert.alert("Ups");
            })
        })
    }

    updateList() {
        var query = "SELECT * from Notes";
        var params = [];

        db.transaction(tx => {
            tx.executeSql(query, params, (tx, results) => {
                console.log(results);
                this.setState({
                    dataList: results.rows._array,
                    refreshing: false
                })
            }, function (tx, error) {
                Alert.alert("Ups");
            })
        })
    }

    handleRefresh = () => {
        this.setState({ dataList: [], refreshing: true }, () => {
            this.updateList();
        })
    }


    deleteData(id) {
        var query = "DELETE FROM Notes WHERE id=?";
        var params = [id];
        db.transaction((tx) => {
            tx.executeSql(query, params, (tx, results) => {

                Alert.alert(i18n.t('removedNoteD'));
            }, function (tx, error) {
                console.log(error);
                Alert.alert("didnt delete :c");
            })
        })
    }
    
    async handleDelete(id) {
        await this.deleteData(id);
        this.fetchData();
    }

    render() {
        // if (this.state.loading) {
        //     return (
        //         <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
        //             <Spinner color="orange"></Spinner>
        //         </View>
        //     );
        // }

        return (
            <SafeAreaView>
                <ScrollView>
                    <View>
                        <FlatList
                            refreshing={this.state.refreshing}
                            onRefresh={this.handleRefresh}
                            data={this.state.dataList}
                            renderItem={(item) => {
                                return (
                                    <View>
                                        <View style={{ flexDirection: 'row' }}>
                                            <Text style={{ fontSize: 20, marginTop: 10, marginBottom: 5, marginLeft: 24, flex: 8 }}>{item.item.title}</Text>
                                            <TouchableOpacity style={{ flex: 1 }} title="Update" onPress={() => {
                                                this.props.mudarNota(item.item.id);
                                                this.props.navigation.navigate('Editar Nota');
                                            }}>
                                                <Icon style={{ marginTop: 7 }} name={'pencil-alt'} size={20} color={'rgba(100,100,100, 0.7)'} />
                                            </TouchableOpacity>
                                            <TouchableOpacity style={{ flex: 1 }} title="Delete" onPress={() => {
                                                this.handleDelete(item.item.id);
                                            }}>
                                                <Icon style={{ marginTop: 7 }} name={'times'} size={20} color={'rgba(100,100,100, 0.7)'} />
                                            </TouchableOpacity>
                                        </View>
                                        <Text style={{ marginBottom: 10, marginLeft: 24 }}>{item.item.description}</Text>
                                        <View style={{ borderBottomColor: 'grey', borderBottomWidth: 1 }}></View>

                                    </View>
                                );
                            }}
                            keyExtractor={(item) => `${item.id}`}
                        />
                    </View>
                </ScrollView>

            </SafeAreaView >
        )
    }

}


