import * as Localization from 'expo-localization';
import i18n from 'i18n-js';

i18n.translations = {
    en: { newAccount: 'Dont have an account? Sign in', 
    or: 'OR', 
    enterGuest: 'Enter as guest',
    notes: 'Notes',
    titleNotes: 'Title',
    descNotes: 'Description',
    saveNotes: 'Save',
    addedNoteT: 'Success!',
    addedNoteD: 'Note successfully saved.',
    newNote: 'New Note',
    editarNota: 'Edit Note',
    removedNoteD: 'Note successfully deleted.'},

    pt: { newAccount: 'Não tem conta? Registe-se', 
    or: 'OU', 
    enterGuest: 'Entrar como convidado',
    notes: 'Noteas',
    titleNotes: 'Titulo',
    descNotes: 'Descrição',
    saveNotes: 'Salvar',
    addedNoteT: 'Sucesso!',
    addedNoteD: 'Nota salva com sucesso.',
    newNote: 'Nova Nota',
    editarNota: 'Editar Nota',
    removedNoteD: 'Nota apagada com sucesso.' }
};

i18n.locale = Localization.locale;
i18n.fallbacks = true;

export default i18n;