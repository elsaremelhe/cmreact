import React from 'react';
import { StyleSheet, Text, View, Button, Alert, Dimensions, TouchableOpacity } from 'react-native';
import { Input } from 'react-native-elements';
import * as SQLite from 'expo-sqlite';
import i18n from 'i18n-js';
//import Icon from 'react-native-vector-icons/Ionicons';

const db = SQLite.openDatabase('CMNotes');
const { width: WIDTH } = Dimensions.get('window');

export default class AddNote extends React.Component {


    constructor(props) {
        super(props);

        this.state = {
            id: props.idNota,
            title: '',
            description: ''
        }
    }


    componentDidMount() {
        this.fetch(this.state.id);
    }

    fetch(id) {
        var query = "SELECT * from Notes where id=?";
        var params = [id];
        db.transaction((tx) => {
            tx.executeSql(query, params, (tx, results) => {
                console.log(results);
                this.setState({
                    title: results.rows._array[0]['title'],
                    description: results.rows._array[0]['description']
                });
            }, function (tx, error) {
                console.log(error);
                Alert.alert("Didnt update :c");
            })
        })
    }

    update(id, title, description) {
        var query = "UPDATE Notes SET title=?, description=? WHERE id=?";
        var params = [title, description, id];
        db.transaction((tx) => {
            tx.executeSql(query, params, (tx, results) => {
                console.log("Update" + results);
                Alert.alert("Nota editada com sucesso");
            }, function (txt, error) {
                console.log(error);
                Alert.alert("didnt update 2");
            })
        })
    }


    handleSave() {

        if (this.state.title != "" && this.state.description != "") {
            this.update(this.state.id, this.state.title, this.state.description);
        } else {
            Alert.alert("Error 1", "File not saved");
        }
    }

    render() {
        return (
            <View style={styles.Nav}>
                <Input onChangeText={(val) => this.setState({ title: val })} value={this.state.title}
                    placeholder='Title'
                    leftIconContainerStyle={{ marginRight: 15 }}
                    inputContainerStyle={{ marginTop: 45, marginLeft: 30, width: 330 }}></Input>

                <Input onChangeText={(val) => this.setState({ description: val })} value={this.state.description}
                    placeholder='Description'
                    leftIconContainerStyle={{ marginRight: 15 }}
                    inputContainerStyle={{ marginTop: 45, marginLeft: 30, width: 330 }}></Input>

                <TouchableOpacity style={styles.btnSave} onPress={() => {
                    this.handleSave();
                }}><Text style={{
                    color: 'white',
                    fontSize: 16,
                    textAlign: 'center'
                  }}>Save</Text></TouchableOpacity>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    Nav: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center'
    },
    btnSave: {
        width: WIDTH - 205,
        height: 45,
        borderRadius: 25,
        backgroundColor: '#FF7E00',
        justifyContent: 'center',
        marginTop: 20
    }
})
