import React, { useEffect, useState } from 'react';
import { StyleSheet, Text, View, Image, TextInput, Dimensions, TouchableOpacity } from 'react-native';
import i18n from 'i18n-js';
import Trad from './Localization';
import axios from 'axios';
import * as Permissions from 'expo-permissions';
import { Camera } from 'expo-camera';
import Icon from 'react-native-vector-icons/FontAwesome5'


const { width: WIDTH } = Dimensions.get('window');

export default class Login extends React.Component {
  constructor(props) {
    super(props)

    camera = null
    console.log(props);
    this.state = {
      idUser: props.route.params.id,
      desc: props.route.params.desc,
      img: props.route.params.img,
      hasCameraPermission: null,
      type: Camera.Constants.Type.back,
      pictureSize: "320x240"
    }

  }

  async componentDidMount() {
    const { status } = await Permissions.askAsync(Permissions.CAMERA);
    this.setState({ hasCameraPermission: status === 'granted' });
  }

  async takePic() {
    if (this.camera) {
        const options = {quality: 1, base64: true};
        const data = await this.camera.takePictureAsync(options);
        //console.log(data.base64);
        this.setState( {img: data.base64} );
    }
}

render() {
  const { hasCameraPermission } = this.state;
  if (hasCameraPermission === null) {
    return <View />;
  } else if (hasCameraPermission === false) {
    return <Text>No access to camera</Text>;
  } else {
    return (
      <View style={styles.background}>

        <View style={styles.inputContainer}>
          <TextInput
            style={styles.input}
            placeholder={Trad.t('descNotes')}
            placeholderTextColor={'rgba(100,100,100, 0.7)'}
            underlineColorAndroid='transparent'
            onChangeText={(text) => this.setState({ desc: text })}
            value={this.state.desc} />
        </View>

        <View style={{height: 450, width: 330, marginTop: 50, marginBottom: 50, alignSelf: 'center'}}>
        <Camera ref={(ref) => (this.camera = ref)} style={{ flex: 1 }} type={this.state.type} pictureSize={this.state.pictureSize}>
            
          </Camera>
        </View>

        <View
              style={{
                backgroundColor: 'transparent',
                flexDirection: 'row'
              }}>
              <TouchableOpacity
                style={styles.btnFlip}
                onPress={() => {
                  this.setState({
                    type:
                      this.state.type === Camera.Constants.Type.back
                        ? Camera.Constants.Type.front
                        : Camera.Constants.Type.back,
                  });
                }}>
                <Icon name={'undo'} size={28} color={'rgba(255,255,255, 1)'} style={styles.icons} />
              </TouchableOpacity>
              <TouchableOpacity style={styles.btnPic} 
                  onPress={()=>{
                    console.log("hello");
                    console.log(this.state.idUser);
                    console.log(this.state.nameUser);
                    console.log(this.state.desc);
                    this.takePic();
                 }}>
                 <Icon name={'camera'} size={28} color={'rgba(255,255,255, 1)'} style={styles.icons} />
            </TouchableOpacity>
            </View>

        <TouchableOpacity style={styles.btnSave} onPress={()=>{
          //console.log(this.state);
          axios({
            method: 'POST',
            url: 'http://192.168.1.5:3000/editmarker',
            data:{
              id: this.state.idUser,
              desc: this.state.desc,
              img: this.state.img
            }
          })
          .then((res)=>{
            this.props.navigation.navigate('Pontos');
            console.log("HEYEYEYEY");
            //console.log(res);
          }, (error)=>{
            console.log('ups' + error);
          })
        }}>
          <Text style={styles.btnText}>{Trad.t('saveNotes')}</Text>
        </TouchableOpacity>

      </View>

    );
  }
}
}

const styles = StyleSheet.create({
  btnFlip: {
      flex: 1,
      height: 50,
      alignItems: 'center',
      marginLeft: 20,
      borderRadius: 25,
      backgroundColor: '#FF7E00'

    },
  btnPic:{
      flex: 1,
      alignItems: 'center',
      marginLeft: 200,
      marginRight: 20,
      borderRadius: 25,
      backgroundColor: '#FF7E00'
  },
  icons: {
      position: 'absolute',
      top: 9
    },
    inputContainer: {
      marginTop: 30,
      marginLeft: 30
    },
    btnSave: {
      width: WIDTH - 205,
      height: 45,
      borderRadius: 25,
      backgroundColor: '#FF7E00',
      justifyContent: 'center',
      marginTop: 40,
      alignSelf: 'center'
    },
    btnText: {
      color: 'white',
      fontSize: 16,
      textAlign: 'center'
    }
});
