import React from 'react';
import { StyleSheet, Text, View, Image, TextInput, Dimensions, TouchableOpacity } from 'react-native';
import i18n from 'i18n-js';
import axios from 'axios';
import Icon from 'react-native-vector-icons/Ionicons'
import logoimg from '../images/loginlogo.jpg'

const { width: WIDTH } = Dimensions.get('window');

export default class Login extends React.Component {
  constructor(props) {
    super(props)

    this.showPass = this.showPass.bind(this);

    this.state = {
      showPass: true,
      press: false,
      pswd: '',
      email: '',
      name: ''
    }

  }

  showPass = () => {
    if (!this.state.press) {
      this.setState({ showPass: false, press: true })
    } else {
      this.setState({ showPass: true, press: false })
    }
  }

  render() {
    return (
      <View style={styles.background}>
        <Image source={logoimg} style={styles.logo}></Image>

        <View style={styles.inputContainer}>
          <Icon name={'ios-mail'} size={28} color={'rgba(100,100,100, 0.7)'} style={styles.icons} />
          <TextInput
            style={styles.input}
            placeholder={'Name'}
            placeholderTextColor={'rgba(100,100,100, 0.7)'}
            underlineColorAndroid='transparent'
            onChangeText={(text) => this.setState({ name: text })}
            value={this.state.name} />
        </View>

        <View style={styles.inputContainer}>
          <Icon name={'ios-mail'} size={28} color={'rgba(100,100,100, 0.7)'} style={styles.icons} />
          <TextInput
            style={styles.input}
            placeholder={'Email'}
            placeholderTextColor={'rgba(100,100,100, 0.7)'}
            underlineColorAndroid='transparent'
            onChangeText={(text) => this.setState({ email: text })}
            value={this.state.email} />
        </View>

        <View style={styles.inputContainer}>
          <Icon name={'md-lock'} size={28} color={'rgba(100,100,100, 0.7)'} style={styles.icons} />
          <TextInput
            style={styles.input}
            placeholder={'Password'}
            secureTextEntry={this.state.showPass}
            placeholderTextColor={'rgba(100,100,100, 0.7)'}
            underlineColorAndroid='transparent'
            onChangeText={(text) => this.setState({ pswd: text })}
            value={this.state.pswd} />

          <TouchableOpacity style={styles.inputEye} onPress={this.showPass.bind(this)}>
            <Icon name={this.state.press ? 'md-eye-off' : 'md-eye'} size={26} color={'rgba(100,100,100, 0.7)'} />
          </TouchableOpacity>
        </View>

        <TouchableOpacity style={styles.btnLogin} onPress={()=>{
          axios({
            method: 'POST',
            url: 'http://192.168.1.5:3000/register',
            data:{
              name: this.state.name,
              email: this.state.email,
              pswd: this.state.pswd
            }
          })
          .then((res)=>{
            alert("Registado com sucesso");
            console.log(res);
          }, (error)=>{
            console.log('ups' + error);
          })
        }}>
          <Text style={styles.btnText}>Registar</Text>
        </TouchableOpacity>

      </View>

    );
  }
}


const styles = StyleSheet.create({
  background: {
    flex: 1,
    width: null,
    height: null,
    alignItems: 'center',
    backgroundColor: 'white'
  },
  logo: {
    width: WIDTH,
    marginTop: 0,
    marginBottom: 0,
    height: 300,
    alignItems: 'flex-start'
  },
  inputContainer: {
    marginTop: 15
  },
  input: {
    width: WIDTH - 55,
    height: 45,
    borderRadius: 25,
    fontSize: 16,
    paddingLeft: 45,
    backgroundColor: 'rgba(255,255,255,0.35)',
    color: 'rgba(0,0,0,0.7)',
    marginHorizontal: 25
  },
  icons: {
    position: 'absolute',
    top: 8,
    left: 37
  },
  inputEye: {
    position: 'absolute',
    top: 8,
    right: 37
  },
  btnLogin: {
    width: WIDTH - 205,
    height: 45,
    borderRadius: 25,
    backgroundColor: '#FF7E00',
    justifyContent: 'center',
    marginTop: 20
  },
  btnText: {
    color: 'white',
    fontSize: 16,
    textAlign: 'center'
  }
});
