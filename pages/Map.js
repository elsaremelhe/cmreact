import React from 'react';
import { View, Text, Image } from 'react-native';
//import {Header, Left, Button, Icon, Body, Title, Right, Fab} from 'native-base';
import  MapView , { Callout } from 'react-native-maps';
//import Meteor, {createContainer} from 'react-native-meteor';
//import Geolocation from '@react-native-community/geolocation';
//import {request, PERMISSIONS} from 'react-native-permissions';
import * as Location from 'expo-location';
import * as Permissions from 'expo-permissions';
import axios from 'axios';
import { TouchableOpacity } from 'react-native-gesture-handler';
import Icon from 'react-native-vector-icons/FontAwesome5';
import { HeaderBackButton } from '@react-navigation/stack';

export default class MapCM extends React.Component {
    constructor(props) {
        super(props);
        this.requestPermission = this.requestPermission.bind(this);

        this.props.navigation.setOptions({
            headerLeft: () => (<View><TouchableOpacity style={{marginLeft: 20, marginRight: 0}} onPress={()=>{
                this.props.navigation.replace('Login')
            }}>
                <Icon name={'power-off'} size={20} color={'white'} />
            </TouchableOpacity></View>),
            headerRight: () => (<View style={{flexDirection: 'row', marginRight: 20}}>
            <TouchableOpacity style={{ marginRight: 24 }} title="Update" onPress={() => {
                axios({
                    method: 'GET',
                    url: 'http://192.168.1.5:3000/getmarkers',
                  })
                  .then((res)=>{
                    this.setState({points: res.data}), () => console.log("AQUI" + this.state.points + "FINITO");
                  }, (error)=>{
                    console.log('ups' + error);
                  })
            }}>
                <Icon name={'undo'} size={20} color={'white'} />
            </TouchableOpacity>
            
            <TouchableOpacity onPress={()=>{
                this.props.navigation.navigate('Pontos', {idUser: this.state.idUser})
            }}>
                <Icon name={'list'} size={20} color={'white'} />
            </TouchableOpacity>
            </View>)
        })

        this.state = {
            idUser: props.idUser,
            nameUser: props.nameUser,
            currentPos: '',
            points: []
          }
    }

    componentDidMount() {
        this.requestPermission();
        axios({
              method: 'GET',
              url: 'http://192.168.1.5:3000/getmarkers',
            })
            .then((res)=>{
              this.setState({points: res.data}), () => console.log("AQUI" + this.state.points.nameUser + "FINITO");
            }, (error)=>{
              console.log('ups' + error);
            })
    }



    async requestPermission() {
        try {
            let res = await Permissions.askAsync(Permissions.LOCATION);

            if (res.granted) {
                let position = await Location.getCurrentPositionAsync({});
                //console.log(JSON.stringify(position));

                let initialPos = {
                    latitude: position.coords.latitude,
                    longitude: position.coords.longitude,
                    latitudeDelta: 0.0922,
                    longitudeDelta: 0.0421,
                }

                this.setState({currentPos: initialPos});
            }
        } catch (error) {
            console.log(error);
        }
    }

    render() {
        return (

            <View style={{ flex: 1 }}>
                <MapView ref={map => this._map = map}
                    style={{
                        flex: 1,
                        zIndex: -1

                    }}
                    
                    initialRegion={
                        this.state.currentPos
                    }
                    showsUserLocation={true}
                    onLongPress={async (e)=>{
                        console.log(this.state.idUser);
                        console.log(this.state.nameUser);
                        // var status = Location.requestPermissionsAsync();

                        // if(status !== "granted") {
                        //     alert("NO PERMISSIONS");
                        //     return;
                        // }

                        // let location = await Location.getCurrentPositionAsync({});


                        console.log(e.nativeEvent.coordinate.latitude);
                        //this.props.enviarID(this.state.idUser),
                        //this.props.enviarNome(this.state.nameUser),
                        this.props.navigation.navigate('Adicionar Ponto', {
                            idUser: this.state.idUser, 
                            nameUser: this.state.nameUser, 
                            lat: e.nativeEvent.coordinate.latitude,
                            long: e.nativeEvent.coordinate.longitude
                        })
                    }}>
                        
                    
                    {
                        this.state.points.map(marker => {
                            //console.log(marker.lat + " " + marker.long);
                           
                            return (<MapView.Marker
                                key={marker._id}
                                coordinate={{latitude: parseFloat(marker.lat), longitude: parseFloat(marker.long) }}
                                >
                                    <MapView.Callout>
                                    <Text style={{fontWeight: "bold", alignSelf: "center"}}>{marker.nameUser}</Text>
                                    <Text style={{alignSelf: "center"}}>{marker.desc}</Text>
                                      <Text style={{marginBottom: 10, height: 120, marginTop: -40}}>
                                      <Image
                                            style={{ width: 110, height: 100, alignSelf: "center"}}
                                            source={{uri: `${marker.img}`}}
                                        />
                                        </Text>  
                                    </MapView.Callout>
                                </MapView.Marker>)
                                }
                            )
                        
                    }

                </MapView>
            </View>
        )
    }
}
