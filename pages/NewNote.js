import React from 'react';
import { StyleSheet, Text, View, Button, Alert, Dimensions } from 'react-native';
import { Input } from 'react-native-elements';
import * as SQLite from 'expo-sqlite';
import i18n from 'i18n-js';
import { TouchableOpacity } from 'react-native-gesture-handler';
//import Icon from 'react-native-vector-icons/Ionicons';

const db = SQLite.openDatabase('CMNotes');
const { width: WIDTH } = Dimensions.get('window');

export default class AddNote extends React.Component {

    state = {
        title: '',
        description: ''
    }


    insert(title, description) {
        var query = "INSERT INTO Notes (title, description) VALUES (?,?)";
        var params = [title, description];
        db.transaction((tx) => {
            tx.executeSql(
                query,
                params,
                (tx, result) => {
                    console.log(result);
                    Alert.alert(i18n.t('addedNoteT'), i18n.t('addedNoteD'));
                }, function (tx, error) {
                    //console.log(error);
                    Alert.alert("Error 2", "File not saved");
                    return;
                },
                null,
                null
            )
        })
    }

    handleSave() {

        if (this.state.title != "" && this.state.description != "") {
            this.insert(this.state.title, this.state.description);
        } else {
            Alert.alert("Error 1", "File not saved");
        }
    }

    render() {
        return (
            <View style={styles.Nav}>
                <Input onChangeText={(val) => this.setState({ title: val })} value={this.state.title}
                    placeholder='Title'
                    leftIconContainerStyle={{ marginRight: 15 }}
                    inputContainerStyle={{ marginTop: 45, marginLeft: 30, width: 330 }}></Input>

                <Input onChangeText={(val) => this.setState({ description: val })} value={this.state.description}
                    placeholder='Description'
                    leftIconContainerStyle={{ marginRight: 15 }}
                    inputContainerStyle={{ marginTop: 45, marginLeft: 30, width: 330 }}></Input>

                <TouchableOpacity style={styles.btnSave} onPress={() => {
                    this.handleSave();
                }}><Text style={{
                    color: 'white',
                    fontSize: 16,
                    textAlign: 'center'
                  }}>{i18n.t('saveNotes')}</Text></TouchableOpacity>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    Nav: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center'
    },
    btnSave: {
        width: WIDTH - 205,
        height: 45,
        borderRadius: 25,
        backgroundColor: '#FF7E00',
        justifyContent: 'center',
        marginTop: 20
    }
})
