import React from 'react';
import { TouchableOpacity, Button } from 'react-native';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';


import Login from './pages/Login';
import Register from './pages/Register';
import NoteList from './pages/NoteList';
import NewNote from './pages/NewNote';
import EditNote from './pages/EditNote';
import Map from './pages/Map';
import AddMarker from './pages/AddMarker';
import Pontos from './pages/MarkerList';
import EditMarker from './pages/EditMarker';

export default class App extends React.Component {
  constructor(props) {
    super(props);

    this.handleIdUser = this.handleIdUser.bind(this);
    this.handleNameUser = this.handleNameUser.bind(this);
    this.handleIdNota = this.handleIdNota.bind(this);
    this.navigateNote = this.navigateNote.bind(this);
    this.handleLat = this.handleLat.bind(this);
    this.handleLong = this.handleLong.bind(this);

    this.state = {
      idNota: -1,
      idUser: -1,
      nameUser: '',
      lat: 0,
      long: 0
    }
  }

  handleIdNota(e) {
    this.setState({ idNota: e });
  }

  handleIdUser(e) {
    this.setState({ idUser: e });
  }

  handleNameUser(e) {
    this.setState({ nameUser: e });
  }

  navigateNote(){
    this.props.navigation.navigate('New Note');
  }

  handleLat(e) {
    this.setState({ lat: e });
  }

  handleLong(e) {
    this.setState({ long: e });
  }

  render() {
    const Stack = createStackNavigator();
    return (
      <NavigationContainer>
        <Stack.Navigator screenOptions={{headerStyle:{backgroundColor: '#FF7E00'}, headerTintColor: 'white' }}>

          <Stack.Screen name="Login" options={{ headerShown: false }}>
            {props => <Login {...props} enviarID={this.handleIdUser} enviarName={this.handleNameUser}/>}
          </Stack.Screen>

          <Stack.Screen name="Register" options={{ headerShown: false }}>
            {props => <Register {...props} />}
          </Stack.Screen>

          <Stack.Screen name="Notas">
            {props => <NoteList {...props} mudarNota={this.handleIdNota} />}
          </Stack.Screen>

          <Stack.Screen name="Nova Nota">
            {props => <NewNote {...props} />}
          </Stack.Screen>

          <Stack.Screen name="Editar Nota">
            {props => <EditNote {...props} idNota={this.state.idNota} />}
          </Stack.Screen>

          <Stack.Screen name="Mapa">
            {props => <Map {...props} idUser={this.state.idUser} nameUser={this.state.nameUser} enviarID={this.handleIdUser} enviarName={this.handleNameUser} 
            enviarLat={this.handleLat} enviarLong={this.handleLong}
            />}
          </Stack.Screen>

          <Stack.Screen name="Adicionar Ponto">
            {props => <AddMarker {...props} idUser={this.state.idUser} nameUser={this.state.nameUser} lat={this.state.lat} long={this.state.long}/>}
          </Stack.Screen>

          <Stack.Screen name="Pontos">
            {props => <Pontos {...props}/>}
          </Stack.Screen>

          <Stack.Screen name="Editar Ponto">
            {props => <EditMarker {...props}/>}
          </Stack.Screen>

        </Stack.Navigator>
      </NavigationContainer>
    );
  }
}
